#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os
import sys
import html


from utils import *

jsonFile = os.path.join(CURRENT_FOLDER, "tmdb_affiches.json")
try:
    movies = getAlreadyScannedMovies(jsonFile)
    if not movies:
        raise ValueError("Aucun film à corriger !")
except:
    print("Erreur : Problème pour lire le fichier {0}.".format(jsonFile))
    sys.exit()

for index, mov in enumerate(movies):
    print()
    print(html.unescape(mov["titre"]))

    updatedMov = getMovieDataById(mov["tmdb_id"])
    if updatedMov["note"] != mov["note"]:
        print(f"\t{mov['note']} => {updatedMov['note']}")
    movies[index].update(updatedMov)


saveMoviesData2Json(jsonFile, CURRENT_FOLDER, movies)
exportMoviesData2Html(CURRENT_FOLDER, movies)
