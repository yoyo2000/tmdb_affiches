#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import sys
from urllib.error import HTTPError

from tmdbv3api import Search

from utils import *

URL_SEARCH = "https://www.themoviedb.org/search/movie?language=fr-FR&query={0}"


def getSearchMovieData(title, year=None):
    initTMDB()

    search = Search()
    foundMovies = search.movies({"query": title, "year": year})
    return [
        {
            "id": movie.id,
            "url": URL_FILM.format(movie.id),
            "title": movie.title,
            "year": movie.get("release_date", "")[0:4],
        }
        for movie in foundMovies
    ]


def main():
    """Appel principal"""
    # Récupérer le dossier de travail et les films à traiter dans les arguments
    if len(sys.argv) <= 2:
        print(
            """Usage : correction_affiches.py "/chemin/vers/dossier/films" "référence du film1" ["référence du film2" ...]

        "chemin/vers/films" => Chemin vers le dossier contenant les vidéos
        "référence du film" => Référence du film à corriger (correspond à l'id de l'affiche et au nom du fichier image sans l'extension)
        """
        )
        sys.exit()

    moviesFolder = sys.argv[1]
    slugMoviesToUpdate = sys.argv[2:]

    tmdbFolder = checkFolder(os.path.join(moviesFolder, TMDB_FOLDER_NAME))
    imagesFolder = checkFolder(os.path.join(tmdbFolder, IMG_FOLDER_NAME))

    # Charger le fichier json
    jsonFile = os.path.join(tmdbFolder, "tmdb_affiches.json")
    try:
        moviesOK = getAlreadyScannedMovies(jsonFile)
        if not moviesOK:
            raise ValueError("Aucun film à corriger !")
        slugMoviesOK = [vu["key"] for vu in moviesOK]
    except:
        print("Erreur : Problème pour lire le fichier {0}.".format(jsonFile))
        sys.exit()

    # Traitement de la liste de film à corriger
    for currentSlug in slugMoviesToUpdate:
        if currentSlug not in slugMoviesOK:
            print("Erreur : Référence '{0}' non trouvée.".format(currentSlug))
            continue

        # Recherche du film
        try:
            currentMovie = next(mov for mov in moviesOK if mov["key"] == currentSlug)
            titleToSearch = currentMovie["filename"]
            print(
                "\nRecherche pour '{0}'".format(titleToSearch),
                f"\n{URL_SEARCH.format(titleToSearch)}",
            )

            foundMovies = getSearchMovieData(titleToSearch)
            for movie in foundMovies:
                print(
                    f"\n\t{movie['id']}:",
                    f"\n\t\t{movie['title']} ({movie['year']})",
                    f"\n\t\t{movie['url']}",
                )

            response = input("\n\tEntrer le code TMDB choisi : ")
            if response is None or response == "":
                print("\t**** Film ignoré ! ****")
                continue

            movieData = getMovieDataById(response)
            if not movieData:
                raise ValueError("no data found with getMovieDataById")
        except:
            print("\t\tErreur : Fiche introuvable pour {0}.".format(currentSlug))
            continue

        indice = moviesOK.index(
            list(filter(lambda x: currentSlug == x["key"], moviesOK))[0]
        )
        moviesOK[indice].update(movieData)

        # Télécharge et enregistre le poster dans le dossier images
        try:
            saveMovieImage(imagesFolder, currentSlug, movieData["url_image"])
        except HTTPError:
            print("\t\tErreur : Image introuvable pour {0}.".format(currentSlug))
            # Le nouveau film n'a pas de poster => supprimer l'ancien
            imgFile = os.path.join(imagesFolder, currentSlug + EXT_IMG)
            if os.path.isfile(imgFile):
                os.remove(imgFile)
            continue

    # Générer le fichier de référence
    saveMoviesData2Json(jsonFile, tmdbFolder, moviesOK)

    # Générer la page html des affiches
    exportMoviesData2Html(tmdbFolder, moviesOK)


if __name__ == "__main__":
    main()
