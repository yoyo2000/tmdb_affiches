#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import html
import os
import sys
from urllib.error import HTTPError

from utils import *

HELP = """
Génère un mur d'affiches de cinéma à partir d'un dossier de films et du site themoviedb.org.
"""


def getArgs():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=HELP,
        epilog="Enjoy ;)",
    )
    parser.add_argument(
        "-r",
        "--recursive",
        action="store_true",
        help="scanne le dossier de films de manière récursive",
    )
    parser.add_argument(
        "-i",
        "--interactif",
        action="store_true",
        help="demande la référence TMDB si aucune fiche n'est trouvée",
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "-d",
        "--date",
        action="store_true",
        help="trie les résultats par date au lieu de l'ordre alphabétique",
    )
    group.add_argument(
        "-n",
        "--note",
        action="store_true",
        help="trie les résultats par note au lieu de l'ordre alphabétique",
    )
    group.add_argument(
        "-a",
        "--annee",
        action="store_true",
        help="trie les résultats par année au lieu de l'ordre alphabétique",
    )
    group.add_argument(
        "-t",
        "--time",
        action="store_true",
        help="trie les résultats par durée au lieu de l'ordre alphabétique",
    )
    parser.add_argument(
        "dossier_films",
        nargs="?",
        help="chemin vers le dossier contenant les films à scanner (dossier courant par défaut)",
        default=CURRENT_FOLDER,
    )
    return parser


def main():
    # Récupérer les arguments de ligne de commande
    parser = getArgs()
    args = parser.parse_args()

    # Récupérer les titres et dates des fichiers vidéos du dossier de film
    moviesFolder = args.dossier_films
    localMoviesData = getLocalMoviesData(moviesFolder, args.recursive)

    # Créer les dossiers de résultat
    if localMoviesData:
        tmdbFolder = createFolderIfNotExists(
            os.path.join(moviesFolder, TMDB_FOLDER_NAME)
        )
        imagesFolder = createFolderIfNotExists(
            os.path.join(tmdbFolder, IMG_FOLDER_NAME)
        )
    else:
        print("Erreur : Aucun film trouvé dans {0}.".format(moviesFolder))
        parser.print_help()
        sys.exit()

    # Récupérer un éventuel précédent scan
    jsonFile = os.path.join(tmdbFolder, "tmdb_affiches.json")
    moviesOK = getAlreadyScannedMovies(jsonFile)
    slugMoviesOK = [vu["key"] for vu in moviesOK]

    # Pour chaque film, récupérer les infos et image d'affiche
    total = len(localMoviesData)
    print("{0} films trouvés :".format(total))
    films = []
    erreurs = []
    for index, localMovie in enumerate(localMoviesData):
        print("#{0}/{1}: {2}".format(index + 1, total, localMovie[0]))
        slugTitle = slugify(localMovie[0])
        if slugTitle in slugMoviesOK:
            # Film déjà scanné
            print("\t\tRéférence : {0}".format(slugTitle))
            film = list(filter(lambda x: slugTitle == x["key"], moviesOK))[0]
            print(
                "\t\tFilm déjà scanné : {0} => {1} ({2})".format(
                    film["filename"], html.unescape(film["titre"]), film["annee"]
                )
            )
            print("\t\tURL : {0}".format(film["url"]))
        else:
            # Recherche le film sur TMDB et récupère les infos
            try:
                movieData = getMovieData(localMovie[0])
            except:
                if args.interactif:
                    reponse = input(
                        "\tEntrer le code TMDB choisi pour {0} : ".format(localMovie[0])
                    )
                    if reponse is None or reponse == "":
                        print("\t*********** Film ignoré ! ***********")
                        continue
                    movieData = getMovieDataById(reponse)
                else:
                    msg = "\t\tErreur : Film introuvable pour {0}.".format(
                        localMovie[0]
                    )
                    print(msg)
                    erreurs.append(msg)
                    continue

            print("\t\t######### : {0}".format(slugTitle))
            print(
                "\t\tFichier : {0} => {1} ({2})".format(
                    localMovie[0], html.unescape(movieData["titre"]), movieData["annee"]
                )
            )
            print("\t\tURL : {0}".format(movieData["url"]))
            film = {}
            film["key"] = slugTitle
            film["filename"] = localMovie[0]
            film["date"] = localMovie[1]
            film.update(movieData)

            # Télécharge et enregistre le poster dans le dossier images
            try:
                saveMovieImage(imagesFolder, slugTitle, movieData["url_image"])
            except HTTPError:
                msg = "\t\tErreur : Image introuvable pour {0}.".format(localMovie[0])
                print(msg)
                erreurs.append(msg)
                continue

        films.append(film)

    if films:
        # Suppression des images correspondant à des films supprimés
        deleteUselessImages(imagesFolder, films)

        # Tri de la liste des films
        if args.date:
            # Classement des films par date d'ajout dans le dossier de vidéos
            films = sorted(films, key=lambda x: x["date"])
        elif args.note:
            # Classement des films par notes décroissantes
            films = sorted(
                films, key=lambda x: x["note"] if x["note"] != "" else "0", reverse=True
            )
        elif args.annee:
            # Classement des films par années décroissantes
            films = sorted(
                films,
                key=lambda x: x["annee"] if x["annee"] != "" else "0",
                reverse=True,
            )
        elif args.time:
            # Classement des films par durée décroissantes
            films = sorted(
                films,
                key=lambda x: x["duree"] if x["duree"] != "" else "0",
                reverse=True,
            )
        else:
            # Classement des films par ordre alphabétique des références
            films = sorted(films, key=lambda x: x["key"])

        # Générer le fichier de référence
        saveMoviesData2Json(jsonFile, tmdbFolder, films)

        # Générer la page html des affiches
        exportMoviesData2Html(tmdbFolder, films)

    # Récapitulatif des erreurs
    if erreurs:
        print("\n")
        print("#" * 50)
        print("Des erreurs se sont produites :")
        for err in erreurs:
            print(err)
        print("#" * 50)


if __name__ == "__main__":
    main()
