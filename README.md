## Description

Scrute un dossier et génère un mur d'affiches de cinéma (page html) avec les fichiers de film trouvés (s'appuie sur une
recherche themoviedb.org).

**Usage :**

```
python3 tmdb_affiches.py -h

usage: tmdb_affiches.py [-h] [-r] [-i] [-d | -n | -a] [dossier_films]

Génère un mur d'affiches de cinéma à partir d'un dossier de films et du site themoviedb.org.

positional arguments:
  dossier_films     chemin vers le dossier contenant les films à scanner (dossier courant par défaut)

optional arguments:
  -h, --help        show this help message and exit
  -r, --recursive   scanne le dossier de films de manière récursive
  -i, --interactif  demande la référence TMDB si aucune fiche n'est trouvée
  -d, --date        trie les résultats par date au lieu de l'ordre alphabétique
  -n, --note        trie les résultats par note au lieu de l'ordre alphabétique
  -a, --annee       trie les résultats par année au lieu de l'ordre alphabétique
```

Crée un dossier **tmdb_affiches** dans le dossier des vidéos. Il contient la page html générée `index.html`, les images
récupérées depuis TMDB (`dossier images`) et un fichier de références `tmdb_affiches.json` permettant de ne pas
re-scanner les films déjà pris en compte.

```
# Se déplacer dans le dossier contenant les films
cd /chemin/vers/dossier/films
python3 /dossier_du_projet/tmdb_affiches/tmdb_affiches.py
```

Pour être trouvés, les fichiers doivent avoir leur nom correspondant au titre du film. Le premier résultat de la
recherche TMDB est choisi d'office. En mode intéractif, si la recherche ne donne aucun résultat, la référence TMDB est
demandée. Dans le mur d'affiches, le survol donne des informations sur le film, le clic mène à la fiche du film sur
TMDB.

Si le premier résultat de TMDB n'est pas le bon pour certains films, un autre script permet de rectifier les erreurs en
proposant de choisir parmis les résultats de la recherche TMDB.

```shell
python3 correction_affiches.py "/chemin/vers/dossier/films" "référence du film1" ["référence du film2" ...]
```

Les références de film correspondent à l'id de l'affiche dans le fichier html ainsi qu'au nom du fichier image sans
l'extension. Lors de la question "_Entrer le code TMDB choisi_", il faut saisir un des numéros inscrits devant chaque
résultat de recherche (c'est le code TMDB du film). On pourra aussi saisir un autre code TMDB même s'il n'est pas
proposé.

Un autre script permet de re-scanner les films déjà scannés pour mettre à jour toutes les fiches de film :

```
# Se déplacer dans le dossier contenant le fichier tmdb_affiches.json
cd /chemin/vers/dossier/films/tmdb_affiches
python3 /dossier_du_projet/tmdb_affiches/update_movies_data.py
```

## Installation

La bibliothèque [tmdbv3api](https://github.com/AnthonyBloomer/tmdbv3api) et le moteur de templating
[Jinja2](http://jinja.pocoo.org/) sont nécessaires.

Pour les installer sous Debian/Ubuntu :

```shell
pip install -r requirements.txt
```

Nécessite une API key que l'on peut obtenir gratuitement ici :
https://developers.themoviedb.org/3/getting-started/introduction. Placer ensuite cette clé dans un fichier nommé
`tmdb_key` ou bien exporter la variable d'environement TMDB_API_KEY :

```
export TMDB_API_KEY='YOUR_API_KEY'
```

Testé sur Ubuntu et Windows.

## License

WTFPL : http://www.wtfpl.net/
