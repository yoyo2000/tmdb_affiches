#!/usr/bin/python3
# -*- coding: utf-8 -*-
import datetime
import glob
import html
import json
import os
import re
import sys
import time
import unicodedata
import webbrowser
from functools import lru_cache
from urllib.request import urlopen

from jinja2 import Environment, FileSystemLoader
from tmdbv3api import Movie, Search, TMDb
from tmdbv3api.exceptions import TMDbException

EXTENSIONS = ["avi", "mp4", "mpeg", "divx", "mkv", "flv"]
EXT_IMG = ".jpg"  # Force cette extension aux fichiers image téléchargés
TMDB_FOLDER_NAME = "tmdb_affiches"
IMG_FOLDER_NAME = "images"

URL_FILM = "https://www.themoviedb.org/movie/{0}?language=fr-FR"
URL_POSTER = "https://image.tmdb.org/t/p/original{0}"

CURRENT_FOLDER = os.getcwd()
RUNTIME_FOLDER = os.path.dirname(os.path.abspath(__file__))

TMDB_EXCEPTION_MESSAGE = "TMDB API error : be sure to put your API key in tmdb_key file or in TMDB_API_KEY environment variable"


def getTmdbApiKey():
    try:
        with open(os.path.join(RUNTIME_FOLDER, "tmdb_key"), "r") as fich:
            return fich.readlines()[0]
    except FileNotFoundError:
        # this file is not mandatory if you store the TMDB_API_KEY environment variable
        pass


@lru_cache(maxsize=1)
def initTMDB():
    """Initialize TMDB only once"""
    tmdb = TMDb()
    apiKeyFromFile = getTmdbApiKey()
    if apiKeyFromFile is not None:
        tmdb.api_key = getTmdbApiKey()
    tmdb.language = "fr"


def slugify(value):
    """
    Normalizes string, converts to lowercase, removes non-alpha characters,
    and converts spaces to hyphens.
    """
    value = unicodedata.normalize("NFKD", value).encode("ascii", "ignore").lower()
    value = re.sub(r"[^\w\s-]", "", value.decode("utf-8")).strip()
    value = re.sub(r" ", "-", value)
    return value


def minutes2Hours(minutes):
    ret = time.strftime("%Hh%M", time.gmtime(minutes * 60))
    return ret if not ret.startswith("0") else ret[1:]


def checkFolder(folderName, fatal=True):
    """Check if a folder exists and if not, exits if fatal is True"""
    if not os.path.isdir(folderName):
        print("Erreur : Le dossier {0} n'existe pas.".format(folderName))
        if fatal:
            sys.exit()
    return folderName


def createFolderIfNotExists(folderName):
    if not os.path.isdir(folderName):
        os.mkdir(folderName)
    return folderName


def getMovieDataById(tmdbId):
    initTMDB()
    try:
        movie = Movie()
        detail = movie.details(tmdbId)
    except TMDbException:
        print(TMDB_EXCEPTION_MESSAGE)
        sys.exit()

    fiche = {}
    fiche["tmdb_id"] = detail.id
    fiche["url"] = URL_FILM.format(detail.id)
    fiche["titre"] = html.escape(detail.title, quote=True)
    fiche["annee"] = detail.get("release_date", "")[0:4]
    fiche["duree"] = minutes2Hours(detail.runtime)
    fiche["genre"] = ", ".join([g.name for g in detail.genres])
    fiche["tagline"] = html.escape(detail.tagline, quote=True)
    fiche["synopsis"] = html.escape(detail.overview, quote=True)
    fiche["note"] = f"{round(detail.vote_average,2)}/10"
    fiche["url_image"] = URL_POSTER.format(detail.poster_path)

    return fiche


def getMovieData(title, year=None):
    initTMDB()
    try:
        search = Search()
    except TMDbException:
        print(TMDB_EXCEPTION_MESSAGE)
        sys.exit()

    firstFoundMovie = search.movies({"query": title, "year": year})[0]
    return getMovieDataById(firstFoundMovie.id)


def getLocalMoviesData(moviesFolder, recursive=False):
    """Returns the local movies list with for each a tupple: (title, date)"""

    checkFolder(moviesFolder)
    if recursive:
        fileList = glob.glob(os.path.join(moviesFolder, "**", "*"), recursive=True)
    else:
        fileList = glob.glob(os.path.join(moviesFolder, "*"))

    moviesData = []
    for element in fileList:
        if not os.path.isdir(element):
            if os.path.splitext(element)[1][-3:] in EXTENSIONS:
                tms = os.path.getmtime(element)
                dte = datetime.datetime.fromtimestamp(tms).strftime("%Y-%m-%d %H:%M:%S")
                moviesData.append(
                    (
                        re.sub(
                            r"[_.]", " ", os.path.splitext(os.path.basename(element))[0]
                        ),
                        dte,
                    )
                )
    return moviesData


def getAlreadyScannedMovies(jsonFile):
    """Returns already scanned movie object list from jsonFile"""
    try:
        with open(jsonFile, "r", encoding="utf-8") as fich:
            moviesOK = json.load(fich)
        return moviesOK
    except:
        return []


def deleteUselessImages(imagesFolder, films):
    imgsOK = [r["key"] for r in films]  # Images de films venant d'être ajoutés
    imgsToVerify = [
        i[:-4]
        for i in os.listdir(imagesFolder)
        if not os.path.isdir(os.path.join(imagesFolder, i))
    ]  # Fichiers image trouvés dans le dossier d'images
    imgsToDelete = [
        i for i in imgsToVerify if i not in imgsOK
    ]  # Images à supprimer (présentes dans imgsToVerify mais pas dans imgsOK)
    for i in imgsToDelete:
        imgFile = os.path.join(imagesFolder, i + EXT_IMG)
        if os.path.isfile(imgFile):
            os.remove(imgFile)


def saveMovieImage(imagesFolder, refImage, urlImage):
    rawImg = urlopen(urlImage).read()
    imageFileName = os.path.join(imagesFolder, refImage + EXT_IMG)
    if os.path.isfile(imageFileName):
        os.remove(imageFileName)
    with open(imageFileName, "wb") as fich:
        fich.write(rawImg)


def saveMoviesData2Json(jsonFile, tmdbFolder, films):
    if os.path.isfile(jsonFile):
        os.replace(
            jsonFile, os.path.join(tmdbFolder, "tmdb_affiches.back.json")
        )  # petit backup
    with open(jsonFile, "w", encoding="utf-8") as fich:
        json.dump(films, fich, ensure_ascii=False, indent=2)


def exportMoviesData2Html(tmdbFolder, films):
    jinja2Env = Environment(loader=FileSystemLoader(RUNTIME_FOLDER), trim_blocks=True)
    with open(os.path.join(tmdbFolder, "index.html"), "wb") as fich:
        fich.write(
            jinja2Env.get_template("tmdb_affiches.html")
            .render(films=films, EXT_IMG=EXT_IMG)
            .encode("utf-8")
        )
        fich.flush()
        time.sleep(3)
        webbrowser.open(fich.name, new=2)
